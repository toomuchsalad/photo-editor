//
//  MediaPickerManager.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 26.07.2022.
//

import Foundation
import UIKit
import Photos

class MediaPickerManager {
    
    static let shared: MediaPickerManager = {
        print(#function)
        let mediaPickerManager = MediaPickerManager()
        mediaPickerManager.requestAuthorization()
        return mediaPickerManager
    }()
    
    var imageManager: PHCachingImageManager?
    var assetsFetchResults: PHFetchResult<PHAsset>?
    
    private func requestAuthorization() {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized, .limited:
                print("Good to proceed")
            case .denied, .restricted:
                print("Not allowed")
            case .notDetermined:
                print("Not determined yet")
            default:
                print("Not determined yet")
            }
            NotificationCenter.default.post(name: .mediaLibraryAccessChanged, object: nil, userInfo: nil)
        }
    }
    
    func loadData(type: PHAssetMediaType) {
        
        imageManager = PHCachingImageManager()
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        options.wantsIncrementalChangeDetails = true
        
        assetsFetchResults = PHAsset.fetchAssets(with: type, options: options)
    }
    
    //, completionHandler: @escaping (UIImage?) -> Void)
    func getPhoto(index: Int, cellSize: CGSize) -> UIImage? {
        
        
        guard let asset = self.assetsFetchResults?[index] else {
            //            completionHandler(nil)
            return nil
        }
        
        let semaphore = DispatchSemaphore(value: 0)
        var finalImage: UIImage?
        
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.isSynchronous = true
        options.resizeMode = .exact
        options.isNetworkAccessAllowed = true
        
        imageManager?.requestImage(for: asset, targetSize: CGSize(width: cellSize.width * 3, height: cellSize.height * 3), contentMode: .aspectFill, options: options, resultHandler: { image, info in
            finalImage = image
            semaphore.signal()
        })
        
        semaphore.wait()
        return finalImage
        
    }
    
    func addPhoto(asset: PHAsset) {
        let imageRequestOptions = PHImageRequestOptions()
        imageRequestOptions.isSynchronous = true
        imageRequestOptions.isNetworkAccessAllowed = true
        PHImageManager.default().requestImage(
            for: asset,
            targetSize: PHImageManagerMaximumSize,
            contentMode: .default,
            options: imageRequestOptions,
            resultHandler: { image, info in
                if let image = image, let imageData = image.pngData() {
                    NewMediaManager.shared.newMedia.insert(MediaModel(imageData: imageData, image: image, textViews: []), at: 0)
                }
            })
        
    }
    
}
