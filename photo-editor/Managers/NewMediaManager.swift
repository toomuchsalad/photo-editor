//
//  MakeMediaManager.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation
import Photos
import UIKit

class NewMediaManager {
    
    static let shared = NewMediaManager()
    
    var newMedia: [MediaModel] = [] {
        didSet {
            NotificationCenter.default.post(name: .newMediaDidChanged, object: nil, userInfo: nil)
        }
    }
    
    func saveImage(preview:UIView) {
        
        let format = UIGraphicsImageRendererFormat()
        format.scale = 6
        let renderer = UIGraphicsImageRenderer(bounds: preview.bounds, format: format)
        preview.layer.cornerRadius = 0
        preview.layoutIfNeeded()
        let image = renderer.image { rendererContext in
            preview.layer.render(in: rendererContext.cgContext)
        }
        preview.layer.cornerRadius = 6
        preview.layoutIfNeeded()
            
        guard let previewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreviewVC") as? PreviewVC else {
            return
        }
        
        UIImageWriteToSavedPhotosAlbum(image, previewVC, #selector(previewVC.saveCompleted(_:didFinishSavingWithError:contextInfo:)), nil)
        
    }
}

