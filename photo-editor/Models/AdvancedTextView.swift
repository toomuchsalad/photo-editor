//
//  AdvancedTextView.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation
import UIKit

class AdvancedTextView: UITextView {

    var id: String!
    var lastSize: CGSize?
    var lastPanPoint: CGPoint?
    var lastPoint: CGPoint?
    var lastTextViewFont:UIFont?
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?

    override var bounds: CGRect {
        didSet {
            self.layer.cornerRadius = bounds.height/4
        }
    }
}
