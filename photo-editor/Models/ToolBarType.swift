//
//  ToolBarType.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation

enum ToolBarType {
    case textColor
    case bgColor
    case font
    case mention
    case none
}
