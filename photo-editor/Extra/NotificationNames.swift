//
//  NotificationNames.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation

extension Notification.Name {
    static let mediaLibraryAccessChanged = Notification.Name(rawValue: "mediaLibraryAccessChanged")
    static let newMediaDidChanged = Notification.Name(rawValue: "newMediaDidChanged")
}
