//
//  MediaPickerVC.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 26.07.2022.
//

import Foundation
import UIKit
import Photos

class MediaPickerVC: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var mediaTypeButton: UIButton!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    var screenType: MediaPickerType = .post
    var mediaType: PHAssetMediaType = .image
    var imageManager: PHCachingImageManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mediaCollectionView.delegate = self
        mediaCollectionView.dataSource = self
        setCollectionLayout()
        
        titleLbl.adjustsFontSizeToFitWidth = true
        switch screenType {
        case .story:
            titleLbl.text = "Добавить в историю"
        case .post:
            titleLbl.text = "Новая публикация"
        }
        
        if #available(iOS 14.0, *) {
            mediaTypeButton.showsMenuAsPrimaryAction = true
            mediaTypeButton.menu = createMenuTypeButton()
        }
        
        reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData), name: .mediaLibraryAccessChanged, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .mediaLibraryAccessChanged, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setCollectionLayout()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setCollectionLayout() {
        let flow = UICollectionViewFlowLayout()
        let width = (UIScreen.main.bounds.width - 4) / 3
        flow.itemSize = CGSize(width: width, height: width)
        flow.minimumLineSpacing = 2
        flow.minimumInteritemSpacing = 2
        flow.scrollDirection = .vertical
        flow.sectionInset = .zero
        
        mediaCollectionView.collectionViewLayout = flow
    }
    
    @objc func reloadData() {
        DispatchQueue.main.async {
            MediaPickerManager.shared.loadData(type: self.mediaType)
            self.mediaCollectionView.reloadData()
        }
        
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func mediaTypeButtonAction(_ sender: UIButton) {
        self.mediaType = mediaType == .image ? .video : .image
        self.mediaTypeButton.setTitle(mediaType == .image ? "Фото" : "Видео", for: .normal)
        self.reloadData()
    }
    
    func createMenuTypeButton() -> UIMenu {
        
        let photosAction = UIAction(title: "Фото") { photo in
            print("photo")
            self.mediaType = .image
            self.reloadData()
        }
        
        let videosAction = UIAction(title: "Видео") { photo in
            print("video")
            self.mediaType = .video
            self.reloadData()
        }
        
        let menu = UIMenu(title: "", children: [photosAction, videosAction])
        
        return menu
    }
}

extension MediaPickerVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (MediaPickerManager.shared.assetsFetchResults?.count ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cameraCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cameraCell", for: indexPath)
            
            return cameraCell
        } else {
            let mediaCell = collectionView.dequeueReusableCell(withReuseIdentifier: "mediaCell", for: indexPath)
            
            let mediaImageView = mediaCell.viewWithTag(101) as? UIImageView
            mediaImageView?.contentMode = .scaleAspectFill
            
            mediaImageView?.image = MediaPickerManager.shared.getPhoto(index: indexPath.row - 1, cellSize: mediaCell.bounds.size)
            
            /*guard let asset = MediaPickerManager.shared.assetsFetchResults?[indexPath.row - 1] else {
                return mediaCell
            }
            MediaPickerManager.shared.imageManager.requestImage(for: asset, targetSize: CGSize(width: mediaCell.bounds.width * 3, height: mediaCell.bounds.height * 3), contentMode: .aspectFill, options: .none, resultHandler: { image, info in
                mediaImageView?.image = image
            })*/
            
            return mediaCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            guard let cameraVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as? CameraVC else {
                print("CameraVC storyboard name error")
                return
            }
            let navi = UINavigationController(rootViewController: cameraVC)
            navi.navigationBar.isHidden = true
            navi.navigationBar.barStyle = .black
            navi.modalPresentationStyle = .fullScreen
            self.present(navi, animated: true)
        } else {
            guard let asset = MediaPickerManager.shared.assetsFetchResults?[indexPath.row - 1] else {
                return
            }
            MediaPickerManager.shared.addPhoto(asset: asset)
            guard let previewVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PreviewVC") as? PreviewVC else {
                print("PreviewVC storyboard name error")
                return
            }
            
            let navi = UINavigationController(rootViewController: previewVC)
            navi.navigationBar.isHidden = true
            navi.navigationBar.barStyle = .black
            navi.modalPresentationStyle = .fullScreen
            self.present(navi, animated: true)
        }
    }
    
}
