//
//  PreviewVC+TextEdit.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation
import UIKit

extension PreviewVC {
    
    func addNewTextView() {
        let textView = AdvancedTextView(frame: CGRect(x: self.previewView.frame.width/2 - 20, y: 100,
                                                      width: 40, height: 40))
        
        textView.id = UUID().uuidString
        //Text Attributes
        textView.textAlignment = .center
        textView.font = UIFont(name: "Helvetica", size: 32)
        textView.textColor = .white
        textView.layer.shadowColor = UIColor.black.cgColor
        textView.layer.shadowOffset = CGSize(width: 1.0, height: 0.0)
        textView.layer.shadowOpacity = 0.2
        textView.layer.shadowRadius = 1.0
        textView.layer.backgroundColor = UIColor.clear.cgColor
        textView.keyboardAppearance = .dark
        textView.layer.cornerRadius = textView.bounds.height/4
        textView.backgroundColor = .black.withAlphaComponent(0.4)
        //
        textView.autocorrectionType = .no
        textView.isScrollEnabled = false
        textView.delegate = self
//        textView.center = previewView.center
        self.previewView.addSubview(textView)
        addGestures(view: textView)
        textView.becomeFirstResponder()
        NewMediaManager.shared.newMedia[mediaIndex].textViews.append(textView)
    }
    
    func addGestures(view: UIView) {
        //Gestures
        view.isUserInteractionEnabled = true
        
        let panGesture = UIPanGestureRecognizer(target: self,
                                                action: #selector(self.panGesture))
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 1
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self,
                                                    action: #selector(self.pinchGesture))
        pinchGesture.delegate = self
        view.addGestureRecognizer(pinchGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture))
        view.addGestureRecognizer(tapGesture)
        
    }
}

extension PreviewVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        if textEditMode {
//            let sizeToFit = textView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude + 4, height:CGFloat.greatestFiniteMagnitude + 4))
//            textView.frame = CGRect(origin: CGPoint(x: 0, y: 100), size: CGSize(width: CGFloat.greatestFiniteMagnitude + 4, height: sizeToFit.height))
            let newSize = textView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
            let newWidth = min(newSize.width, self.previewView.frame.width - 32)
            
            textView.frame = CGRect(x: self.previewView.frame.width/2 - newWidth/2, y: 100, width: newWidth, height: newSize.height)
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let advancedTextView = textView as? AdvancedTextView, let existTextView = NewMediaManager.shared.newMedia[mediaIndex].textViews.filter({$0.id == advancedTextView.id}).first else {
            return
        }
        
        if !textEditMode {
            textEditMode = true
        }
        
        existTextView.lastTextViewTransform = textView.transform
        existTextView.lastTextViewTransCenter = textView.center
        existTextView.lastTextViewFont = textView.font!
        //        activeTextView = textView
        textView.superview?.bringSubviewToFront(textView)
        textView.font = UIFont(name: "Helvetica", size: 32)
        UIView.animate(withDuration: 0.3, animations: {
            textView.transform = CGAffineTransform.identity
            textView.frame = CGRect(x: self.previewView.frame.width/2 - textView.frame.width/2, y: 100, width: textView.frame.width, height: textView.frame.height)
            
        })
        
//        textView.translatesAutoresizingMaskIntoConstraints = false
//        textView.centerYAnchor.constraint(equalTo: self.previewView.centerYAnchor).isActive = true
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let advancedTextView = textView as? AdvancedTextView, let existTextView = NewMediaManager.shared.newMedia[mediaIndex].textViews.filter({$0.id == advancedTextView.id}).first else {
            return
        }
        
        guard let lastTextViewTransform = existTextView.lastTextViewTransform, let lastTextViewTransCenter = existTextView.lastTextViewTransCenter, let lastTextViewFont = existTextView.lastTextViewFont else {
            return
        }
        //        activeTextView = nil
        textView.font = lastTextViewFont
        UIView.animate(withDuration: 0.3, animations: {
            textView.transform = lastTextViewTransform
            textView.center = lastTextViewTransCenter
        }, completion: nil)
//        let sizeToFit = textView.sizeThatFits(CGSize(width: UIScreen.main.bounds.width - 64, height:CGFloat.greatestFiniteMagnitude + 4))
//        textView.frame.size = CGSize(width: UIScreen.main.bounds.width - 64, height: sizeToFit.height)
        let newSize = textView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: newSize.width, height: newSize.height)
    }
    
}
