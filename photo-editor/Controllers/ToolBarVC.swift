//
//  ToolBarVC.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 28.07.2022.
//

import Foundation
import UIKit

class ToolBarVC: UIViewController {
    
    @IBOutlet var bottomToolBarView: UIVisualEffectView!
    @IBOutlet weak var mentionButton: UIButton!
    @IBOutlet weak var linkButton: UIButton!
    
    @IBOutlet weak var toolsCollectionView: UICollectionView!
    
    @IBOutlet weak var mentionEffectView: UIVisualEffectView!
    @IBOutlet weak var mentionCollectionView: UICollectionView!
    
    var toolBarType: ToolBarType = .none {
        didSet {
            setType()
        }
    }
    
    var textColors:[UIColor] = [.white, .black, .gray, .blue, .yellow, .red, .green]
    var bgColors:[UIColor] = [.white, .black, .gray]
    var fonts:[UIFont] = [UIFont.systemFont(ofSize: 14, weight: .black), UIFont.systemFont(ofSize: 14, weight: .bold), UIFont.systemFont(ofSize: 14, weight: .medium), UIFont.systemFont(ofSize: 14, weight: .light), UIFont.systemFont(ofSize: 14, weight: .ultraLight)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toolsCollectionView.delegate = self
        toolsCollectionView.dataSource = self
        
        mentionCollectionView.delegate = self
        mentionCollectionView.dataSource = self
        
        
    }
    
    func setType() {
        if bottomToolBarView != nil, toolsCollectionView != nil, mentionEffectView != nil {
            switch toolBarType {
            case .textColor, .bgColor, .font:
                bottomToolBarView.isHidden = false
                toolsCollectionView.isHidden = false
                mentionEffectView.isHidden = true
            case .mention:
                bottomToolBarView.isHidden = true
                toolsCollectionView.isHidden = true
                mentionEffectView.isHidden = true
            case .none:
                bottomToolBarView.isHidden = false
                toolsCollectionView.isHidden = true
                mentionEffectView.isHidden = true
            }
            toolsCollectionView.reloadData()
            mentionCollectionView.reloadData()
        }
    }
    
}

extension ToolBarVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case toolsCollectionView:
            switch toolBarType {
            case .bgColor:
                return bgColors.count
            case .font:
                return fonts.count
            case .textColor:
                return textColors.count
            default:
                return 0
            }
        case mentionCollectionView:
            return 7
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case toolsCollectionView:
            switch toolBarType {
            case .bgColor, .textColor:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCell", for: indexPath)
                
                let smallView = cell.viewWithTag(401)
                smallView?.layer.cornerRadius = 13
                smallView?.clipsToBounds = true
                let bigView = cell.viewWithTag(402)
                bigView?.layer.cornerRadius = 18
                bigView?.clipsToBounds = true
                
                let color = toolBarType == .textColor ? textColors[indexPath.row] : bgColors[indexPath.row]
                
                if indexPath.row == 0 {
                    smallView?.backgroundColor = color
                    bigView?.backgroundColor = .clear
                    bigView?.layer.borderColor = UIColor.white.cgColor
                    bigView?.layer.borderWidth = 2
                } else {
                    smallView?.backgroundColor = .clear
                    bigView?.backgroundColor = color
                    bigView?.layer.borderColor = UIColor.clear.cgColor
                    bigView?.layer.borderWidth = 0
                }
                
                return cell
            case .font:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fontCell", for: indexPath)
                
                let effectView = cell.viewWithTag(301) as? UIVisualEffectView
                effectView?.layer.cornerRadius = 18
                effectView?.clipsToBounds = true
                let blankView = cell.viewWithTag(302)
                blankView?.layer.cornerRadius = 18
                blankView?.clipsToBounds = true
                let fontLbl = cell.viewWithTag(303) as? UILabel
                
                let font = fonts[indexPath.row]
                fontLbl?.font = font
                
                if indexPath.row == 0 {
                    effectView?.isHidden = true
                    blankView?.isHidden = false
                    fontLbl?.textColor = .blue
                } else {
                    effectView?.isHidden = false
                    blankView?.isHidden = true
                    fontLbl?.textColor = .white
                }
                
                return cell
            default:
                return UICollectionViewCell()
            }
        case mentionCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mentionCell", for: indexPath)
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
}
