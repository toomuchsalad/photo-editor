//
//  PreviewVC.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation
import UIKit
import AVFoundation

class PreviewVC: UIViewController {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var imageTintView: UIView!
    
    @IBOutlet weak var previewCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var mainToolsView: UIView!
    
    @IBOutlet weak var textEditToolsView: UIView!
    @IBOutlet weak var textDoneButton: UIButton!
    @IBOutlet weak var textAlignButton: UIButton!
    @IBOutlet weak var textColorButton: UIButton!
    @IBOutlet weak var textBgColorButton: UIButton!
    @IBOutlet weak var textFontButton: UIButton!
    
    @IBOutlet weak var deleteImageView: UIImageView!
    
    var mediaIndex: Int = 0
    var textEditMode: Bool = false {
        didSet {
            if !textEditMode {
                view.endEditing(true)
                toolBarType = .none
            }
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) { [self] in
                    imageTintView.alpha = textEditMode ? 1 : 0
                    mainToolsView.alpha = textEditMode ? 0 : 1
                    textEditToolsView.alpha = textEditMode ? 1 : 0
                }
            }
            
        }
    }
    var toolBarType: ToolBarType = .none {
        didSet {
            updateTextButons()
            for child in self.children {
                if let vc = child as? ToolBarVC {
                    vc.toolBarType = toolBarType
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let roundButtons: [UIButton] = [soundButton, textButton, downloadButton, nextButton, textAlignButton, textColorButton, textBgColorButton, textFontButton]
        roundButtons.forEach { button in
            button.layer.cornerRadius = 20
            button.clipsToBounds = true
        }
        textColorButton.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        textBgColorButton.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        textFontButton.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        
        
        previewView.layer.cornerRadius = 6
        previewView.clipsToBounds = true
        
        previewCollectionView.delegate = self
        previewCollectionView.dataSource = self
        
        textEditToolsView.alpha = 0
        imageTintView.alpha = 0
        
        setImage()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setImage), name: .newMediaDidChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .newMediaDidChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func setImage() {
        previewImageView.image = NewMediaManager.shared.newMedia[mediaIndex].image
        previewCollectionView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if textEditMode {
            textEditMode = false
        }
    }
    
    @objc func saveCompleted(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            showAlert(title: "Save Error", body: error.localizedDescription)
        } else {
            showAlert(title: "Save Successfull", body: nil)
        }
    }
    
    //main buttons
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func soundButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func textButtonAction(_ sender: Any) {
        textEditMode = true
        addNewTextView()
    }
    
    @IBAction func downloadButtonAction(_ sender: Any) {
        NewMediaManager.shared.saveImage(preview: self.previewView)
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
    }
    
    //text edit buttons
    @IBAction func textDoneButtonAction(_ sender: Any) {
        textEditMode = false
    }
    
    @IBAction func textAlignButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func textColorButtonAction(_ sender: Any) {
        toolBarType = toolBarType == .textColor ? .none : .textColor
    }
    
    @IBAction func textBgButtonAction(_ sender: Any) {
        toolBarType = toolBarType == .bgColor ? .none : .bgColor
    }
    
    @IBAction func textFontButtonAction(_ sender: Any) {
        toolBarType = toolBarType == .font ? .none : .font
    }
    
    func updateTextButons() {
        switch toolBarType {
        case .textColor:
            textColorButton.backgroundColor = .white.withAlphaComponent(0.3)
            textColorButton.layer.borderWidth = 1
            textBgColorButton.backgroundColor = .white.withAlphaComponent(0.1)
            textBgColorButton.layer.borderWidth = 0
            textFontButton.backgroundColor = .white.withAlphaComponent(0.1)
            textFontButton.layer.borderWidth = 0
        case .bgColor:
            textColorButton.backgroundColor = .white.withAlphaComponent(0.1)
            textColorButton.layer.borderWidth = 0
            textBgColorButton.backgroundColor = .white.withAlphaComponent(0.3)
            textBgColorButton.layer.borderWidth = 1
            textFontButton.backgroundColor = .white.withAlphaComponent(0.1)
            textFontButton.layer.borderWidth = 0
        case .font:
            textColorButton.backgroundColor = .white.withAlphaComponent(0.1)
            textColorButton.layer.borderWidth = 0
            textBgColorButton.backgroundColor = .white.withAlphaComponent(0.1)
            textBgColorButton.layer.borderWidth = 0
            textFontButton.backgroundColor = .white.withAlphaComponent(0.3)
            textFontButton.layer.borderWidth = 1
        case .mention, .none:
            textColorButton.backgroundColor = .white.withAlphaComponent(0.1)
            textColorButton.layer.borderWidth = 0
            textBgColorButton.backgroundColor = .white.withAlphaComponent(0.1)
            textBgColorButton.layer.borderWidth = 0
            textFontButton.backgroundColor = .white.withAlphaComponent(0.1)
            textFontButton.layer.borderWidth = 0
        }
    }
    
}

extension PreviewVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NewMediaManager.shared.newMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "previewCell", for: indexPath)
        
        let previewImageView = cell.viewWithTag(201) as? UIImageView
        let selectedView = cell.viewWithTag(202)
        
        previewImageView?.layer.cornerRadius = 8
        previewImageView?.clipsToBounds = true
        
        selectedView?.layer.cornerRadius = 10
        selectedView?.layer.borderColor = UIColor.white.cgColor
        selectedView?.layer.borderWidth = 1
        
        selectedView?.isHidden = indexPath.row == mediaIndex ? false : true
        
        previewImageView?.image = NewMediaManager.shared.newMedia[indexPath.row].image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        mediaIndex = indexPath.row
        self.setImage()
    }
    
}
