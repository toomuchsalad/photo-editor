//
//  PreviewVC+ToolBar.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation
import UIKit

extension PreviewVC {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else {
            return
        }
        
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ToolBarVC") as? ToolBarVC else { return }
        addChild(vc)
        
        UIView.performWithoutAnimation {
            vc.toolBarType = self.toolBarType
            vc.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - keyboardSize.height - 100, width: UIScreen.main.bounds.width, height: 100)
            view.addSubview(vc.view)
            
            vc.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
            vc.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            vc.view.heightAnchor.constraint(equalToConstant: 100).isActive = true
            vc.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
            
                    vc.view.transform = CGAffineTransform(translationX: 0, y: keyboardSize.height+100)
        }
        
        UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseInOut, animations: {
            vc.view.transform = .identity
        }, completion: { _ in
            vc.didMove(toParent: self)
        })
        
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else {
            return
        }
        
        print("CHILDREN", self.children)
        for child in self.children {
            if child is ToolBarVC {
                
                UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseInOut, animations: {
                    child.view.transform = CGAffineTransform(translationX: 0, y: keyboardSize.height + 100)
                }, completion: { _ in
                    child.willMove(toParent: nil)
                    child.view.removeFromSuperview()
                    child.removeFromParent()
                    child.view.transform = .identity
                })
                
                
            }
        }
    }
    
}
