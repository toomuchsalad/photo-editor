//
//  PreviewVC+Ges.swift
//  photo-editor
//
//  Created by Andrey Atroshchenko on 27.07.2022.
//

import Foundation
import UIKit

extension PreviewVC: UIGestureRecognizerDelegate {
    //Translation is moving object
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        if !textEditMode, let view = recognizer.view {
            moveView(view: view, recognizer: recognizer)
        }
    }
    
    @objc func pinchGesture(_ recognizer: UIPinchGestureRecognizer) {
        if !textEditMode, let view = recognizer.view {
            guard let textView = view as? AdvancedTextView, let existTextView = NewMediaManager.shared.newMedia[mediaIndex].textViews.filter({$0.id == textView.id}).first else {
                return
            }
            
            let font = UIFont(name: textView.font!.fontName, size: textView.font!.pointSize * recognizer.scale)
            existTextView.font = font
            textView.font = font
            
            let sizeToFit = textView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
            
            textView.bounds.size = sizeToFit
            existTextView.lastSize = sizeToFit
            
            textView.setNeedsDisplay()
            
            recognizer.scale = 1
        }
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        if !textEditMode, let view = recognizer.view {
            if let textView = view as? UITextView {
                textView.becomeFirstResponder()
                if #available(iOS 10.0, *) {
                    let generator = UIImpactFeedbackGenerator(style: .heavy)
                    generator.impactOccurred()
                }
            }
        }
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func moveView(view: UIView, recognizer: UIPanGestureRecognizer)  {
        
        guard let textView = view as? AdvancedTextView, let existTextView = NewMediaManager.shared.newMedia[mediaIndex].textViews.filter({$0.id == textView.id}).first else {
            return
        }
        
        UIView.animate(withDuration: 0.3) {
            self.deleteImageView.alpha = 0.7
        }
        
        view.superview?.bringSubviewToFront(view)
        let pointToSuperView = recognizer.location(in: self.view)
        
        view.center = CGPoint(x: view.center.x + recognizer.translation(in: previewView).x, y: view.center.y + recognizer.translation(in: previewView).y)
        
        recognizer.setTranslation(CGPoint.zero, in: previewView)
        
        if let previousPoint = existTextView.lastPanPoint {
            //View is going into deleteView
            if deleteImageView.frame.contains(pointToSuperView) && !deleteImageView.frame.contains(previousPoint) {
                if #available(iOS 10.0, *) {
                    let generator = UIImpactFeedbackGenerator(style: .heavy)
                    generator.impactOccurred()
                }
                UIView.animate(withDuration: 0.3, animations: {
                    view.transform = view.transform.scaledBy(x: 0.25, y: 0.25)
                    view.center = recognizer.location(in: self.previewView)
                })
            }
            //View is going out of deleteView
            else if deleteImageView.frame.contains(previousPoint) && !deleteImageView.frame.contains(pointToSuperView) {
                //Scale to original Size
                UIView.animate(withDuration: 0.3, animations: {
                    view.transform = view.transform.scaledBy(x: 4, y: 4)
                    view.center = recognizer.location(in: self.previewView)
                })
            }
        }
        existTextView.lastPanPoint = pointToSuperView
        
        if recognizer.state == .ended {
            existTextView.lastPanPoint = nil
            UIView.animate(withDuration: 0.3) {
                self.deleteImageView.alpha = 0
            }
            let point = recognizer.location(in: self.view)
            existTextView.lastPoint = point
            
            if deleteImageView.frame.contains(point) { // Delete the view
                view.removeFromSuperview()
                NewMediaManager.shared.newMedia[mediaIndex].textViews = NewMediaManager.shared.newMedia[mediaIndex].textViews.filter({$0.id != existTextView.id})
                if #available(iOS 10.0, *) {
                    let generator = UINotificationFeedbackGenerator()
                    generator.notificationOccurred(.success)
                }
            } /*else if !previewView.bounds.contains(view.center) { //Snap the view back to tempimageview
                UIView.animate(withDuration: 0.3, animations: {
                    view.center = self.previewView.center
                })
                
            }*/
        }
    }
}
